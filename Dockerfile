FROM python:3.9-slim
COPY requirements.txt /
RUN python -m pip install --no-cache-dir -r /requirements.txt

WORKDIR /app
COPY setup.py setup.py
COPY resif_datacite_cli resif_datacite_cli

RUN pip install -e /app
