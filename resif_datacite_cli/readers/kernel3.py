# coding: utf-8
import os
import logging
from lxml import etree
from .errors import ReaderErrorDocumentInvalid
from . import ReaderAbstract, CustomElementBase

# Configure logger
logger = logging.getLogger()


class ReaderKernel3(ReaderAbstract):
    """
    XML DataCite reader for schema v3.x
    """

    # Constructor
    def __init__(self, schema_version):
        super(ReaderKernel3, self).__init__(schema_version, 3)
        self._parser.set_element_class_lookup(etree.ElementDefaultClassLookup(element=CustomElementBaseKernel3))

    def validate(self, xml_tree):
        """
        Validate a XML tree

        :param xml_tree: The XML tree object to validate
        :rtype: bool
        """
        logger.debug("ReaderKernel3.validate(%s)" % xml_tree)
        try:
            self._schema.assertValid(xml_tree)
            return True
        except etree.DocumentInvalid as e:
            message = str(e).replace('{http://datacite.org/schema/kernel-3}', '')
            raise ReaderErrorDocumentInvalid(message)


class CustomElementBaseKernel3(CustomElementBase):
    """
    LXML Customized ElementBase for schema version 3.x
    """

    def _find(self, path):
        return self.find('{http://datacite.org/schema/kernel-3}%s' % path, namespaces={None: 'http://datacite.org/schema/kernel-3'})

    def _iterchildren(self, tag=None):
        if tag is not None:
            return self.iterchildren('{http://datacite.org/schema/kernel-3}%s' % tag)
        else:
            return self.iterchildren()
