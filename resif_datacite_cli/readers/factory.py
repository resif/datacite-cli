# coding: utf-8
import logging
from .kernel3 import ReaderKernel3
from .kernel4 import ReaderKernel4
from .errors import ReaderFactoryError

# Configure logger
logger = logging.getLogger()


class ReaderFactory(object):
    """
    Factory of XML reader objects
    """

    @classmethod
    def factory(cls, schema_version):
        """
        Build a Reader object
        :param schema_version: The DataCite XML Schema version
        :return: An initialized Reader object
        :rtype: ReaderKernel4|ReaderKernel3
        """
        if float(schema_version) >= 4:
            return ReaderKernel4(schema_version)
        elif float(schema_version) >= 3:
            return ReaderKernel3(schema_version)
        else:
            raise ReaderFactoryError("The DataCite schema version '%s' is not supported yet" % schema_version)
