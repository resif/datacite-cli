# coding: utf-8
import os
import logging
from lxml import etree
from .errors import ReaderErrorDocumentInvalid
from . import ReaderAbstract, CustomElementBase

# Configure logger
logger = logging.getLogger()


class ReaderKernel4(ReaderAbstract):
    """
    XML DataCite reader for schema v4.x
    """

    # Constructor
    def __init__(self, schema_version):
        super(ReaderKernel4, self).__init__(schema_version, 4)
        self._parser.set_element_class_lookup(etree.ElementDefaultClassLookup(element=CustomElementBaseKernel4))

    def validate(self, xml_tree):
        """
        Validate a XML tree

        :param xml_tree: The XML tree object to validate
        :rtype: bool
        """
        logger.debug("ReaderKernel4.validate(%s)" % xml_tree)
        try:
            self._schema.assertValid(xml_tree)
            return True
        except etree.DocumentInvalid as e:
            message = str(e).replace('{http://datacite.org/schema/kernel-4}', '')
            raise ReaderErrorDocumentInvalid(message)


class CustomElementBaseKernel4(CustomElementBase):
    """
    LXML Customized ElementBase for schema version 4.x
    """

    def _find(self, path):
        return self.find('{http://datacite.org/schema/kernel-4}%s' % path, namespaces={None: 'http://datacite.org/schema/kernel-4'})

    def _iterchildren(self, tag=None):
        if tag is not None:
            return self.iterchildren('{http://datacite.org/schema/kernel-4}%s' % tag)
        else:
            return self.iterchildren()
