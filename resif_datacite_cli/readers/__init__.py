# coding: utf-8
import os
import logging
from lxml import etree
from .errors import ReaderErrorDocumentInvalid

# Configure logger
logger = logging.getLogger()


class ReaderAbstract(object):
    """
    Base class for XML file readers
    """

    # Constructor
    def __init__(self, schema_version, kernel_version):
        self._parser = etree.XMLParser(remove_comments=True, remove_pis=True)
        self._schema = etree.XMLSchema(file=os.path.abspath(os.path.join(os.path.dirname(__file__), '../', 'schemas', str(schema_version), 'metadata.xsd')))
        self._schema_version = schema_version
        self._kernel_version = kernel_version

    @staticmethod
    def read(file_path):
        """
        Read the content of a file

        :param file_path: The file path
        :return: The file content
        :rtype: str
        """
        logger.debug("ReaderAbstract.read(%s)" % file_path)
        content = None
        with open(file_path, 'r') as f:
            content = f.read()
        return content

    def parse(self, file_path, validate=True):
        """
        Parse a XML file
        :param file_path: The file path
        :param validate: Enable the XML structure validation
        :return: etree.ElementTree
        """
        logger.debug("ReaderAbstract.parse(%s)" % file_path)
        try:
            xml_tree = etree.parse(file_path, parser=self._parser)
        except etree.XMLSyntaxError as e:
            raise ReaderErrorDocumentInvalid(e)

        if validate:
            self.validate(xml_tree)
        return xml_tree

    def validate(self, xml_tree):
        """
        Validate an XML structure

        :param xml_tree: The XML tree to validate
        :rtype: bool
        """
        logger.debug("ReaderAbstract.validate(%s)" % xml_tree)
        return NotImplemented

    def is_valid(self, xml_tree):
        """
        Check if a XML structure is valid

        :param xml_tree: The XML tree to validate
        :rtype: bool
        """
        logger.debug("ReaderAbstract.is_valid(%s)" % xml_tree)
        try:
            return self.validate(xml_tree)
        except ReaderErrorDocumentInvalid:
            return False

    @staticmethod
    def get_identifier(xml_tree):
        """
        Get the DOI identifier from the XML

        :param xml_tree: The XML tree
        :return: The DOI identifier
        :rtype: str
        """
        logger.debug("ReaderAbstract.get_identifier(%s)" % xml_tree)
        xml_identifier = xml_tree.getroot()._find('identifier')
        if xml_identifier is not None:
            identifier = str(xml_identifier.text).strip()
            logger.debug("Identifier found: %s" % identifier)
            return identifier
        else:
            logger.error("Unable to find an identifier")  #FIXME


# XML Element Bases
########################################################################################################################

class CustomElementBase(etree.ElementBase):
    """
    LXML Customized ElementBase
    """

    @property
    def _tag(self):
        return self.tag.rsplit('}', 1)[-1]
