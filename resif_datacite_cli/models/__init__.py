# coding: utf-8
import logging
from .errors import DoiErrorIdentifierInvalid

# Configure logger
logger = logging.getLogger()


class DoiAbstract(object):
    """
    Base class for DOI content structures
    """

    # Constructor
    def __init__(self, identifier, content):
        self.identifier = identifier
        self.content = content

    def location(self, base_url='http://seismology.resif.fr'):
        """
        Build the landing page URL from a base URL (according to the new portal)

        :param base_url: The base URL to use (default='http://seismology.resif.fr')
        :return: The landing page URL
        :rtype: str
        """
        logger.debug("DoiAbstract.location(%s)" % base_url)
        return NotImplemented

    @property
    def is_valid(self):
        """
        Check if the DOI is valid

        :rtype: bool
        """
        logger.debug("DoiAbstract.is_valid()")
        try:
            return self.validate()
        except DoiErrorIdentifierInvalid:
            return False

    def validate(self):
        """
        Validate the DOI

        :rtype: bool
        """
        logger.debug("DoiAbstract.validate()")

        # Check the identifier
        if not self._validate_identifier():
            logger.debug("The '%s' identifier is INVALID" % self.identifier)
            raise DoiErrorIdentifierInvalid("The '%s' identifier is invalid" % self.identifier)
        else:
            logger.debug("The '%s' identifier is VALID" % self.identifier)
            return True

    def _validate_identifier(self):
        """
        Validate the identifier

        :rtype: bool
        """
        logger.debug("DoiAbstract._validate_identifier()")
        return NotImplemented
