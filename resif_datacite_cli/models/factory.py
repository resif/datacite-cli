# coding: utf-8
import logging
from ..readers.errors import ReaderError
from .network import DoiResifNetwork
from .errors import DoiFactoryError

# Configure logger
logger = logging.getLogger()


class DoiFactory(object):
    """
    Factory of DOI objects
    """

    TYPE_RESIF_NETWORK = 'network'

    @classmethod
    def factory(cls, reader, file_path, file_type=TYPE_RESIF_NETWORK):
        """
        Build a DOI object from a XML file

        :param reader: The Reader object
        :param file_path: The XML file path
        :param file_type: The file type (default='network')
        :return: An initialized DOI object
        :rtype: DoiResifNetwork
        """
        logger.debug("DoiFactory.factory(%s, %s, %s)" % (reader, file_path, file_type))
        try:
            content = reader.read(file_path)
            xml_tree = reader.parse(file_path)
            identifier = reader.get_identifier(xml_tree)
        except ReaderError:
            raise

        if file_type == cls.TYPE_RESIF_NETWORK:
            return DoiResifNetwork(identifier, content)
        else:
            raise DoiFactoryError("DOI file type '%s' not supported yet" % file_type)
