# coding: utf-8
from ..errors import Error


class ModelError(Error):
    """
    Base class for errors raised by the models
    """
    pass


class DoiFactoryError(ModelError):
    """
    Factory error
    """
    pass


class DoiErrorIdentifierInvalid(ModelError):
    """
    Identifier invalid
    """
    pass
