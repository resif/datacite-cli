#!/usr/bin/env python
# encoding: utf8
import os
import sys
import click
import click_log
import logging
from .service import ServiceDoi
from dotenv import load_dotenv

# Configure logger
logging.SUCCESS = 25  # between WARNING and INFO
logging.addLevelName(logging.SUCCESS, 'SUCCESS')

logger = logging.getLogger()
setattr(logger, 'success', lambda message, *args: logger._log(logging.SUCCESS, message, args))

click_log.basic_config(logger)
click_log.ColorFormatter.colors['debug'] = dict(fg="white")
click_log.ColorFormatter.colors['info'] = dict(fg="blue")
click_log.ColorFormatter.colors['success'] = dict(fg="green")


# Load environment from ".env" file
load_dotenv()


@click.group()
def cli():
    pass


# CHECK
########################################################################################################################

@cli.command(help='Validate a DataCite XML file')
@click_log.simple_verbosity_option(logger)
@click.argument('path', type=click.Path(exists=True))
@click.option('--prefix', envvar='DATACITE_PREFIX', help='DataCite account prefix')
@click.option('--login', envvar='DATACITE_LOGIN', help='DataCite account login')
@click.option('--password', envvar='DATACITE_PASSWORD', prompt=True, help='DataCite account password')
@click.option('--test', envvar='DATACITE_TEST', is_flag=True, help='Use the DataCite TEST platform')
@click.option('--schema', envvar='DATACITE_SCHEMA_VERSION', default='4.4', help='DataCite schema version')
@click.option('--baseurl', envvar='DATACITE_BASE_URL', default='http://seismology.resif.fr', help='Base URL for landing pages')
@click.option('--stop/--no-stop', 'stop', default=True, help='Stop execution on first error')
@click.option('--offline', is_flag=True, help='Run only offline validation steps')
def validate(path, prefix, login, password, test, schema, baseurl, stop, offline):
    service = ServiceDoi(prefix=prefix, login=login, password=password, test=test, schema_version=schema, base_url=baseurl)
    success = service.validate(path, stop, offline)

    if not success:
        sys.exit(os.EX_SOFTWARE)
    else:
        sys.exit(os.EX_OK)

# REGISTER/UPDATE
########################################################################################################################

@cli.command(help='Register/Update a DOI on DataCite')
@click_log.simple_verbosity_option(logger)
@click.argument('path', type=click.Path(exists=True))
@click.option('--prefix', envvar='DATACITE_PREFIX', help='DataCite account prefix')
@click.option('--login', envvar='DATACITE_LOGIN', help='DataCite account login')
@click.option('--password', envvar='DATACITE_PASSWORD', prompt=True, help='DataCite account password')
@click.option('--test', envvar='DATACITE_TEST', is_flag=True, help='Use the DataCite TEST platform')
@click.option('--schema', envvar='DATACITE_SCHEMA_VERSION', default='4.4', help='DataCite schema version')
@click.option('--baseurl', envvar='DATACITE_BASE_URL', default='http://seismology.resif.fr', help='Base URL for landing pages')
@click.option('--stop/--no-stop', 'stop', default=True, help='Stop execution on first error')
def upload(path, prefix, login, password, test, schema, baseurl, stop):
    service = ServiceDoi(prefix=prefix, login=login, password=password, test=test, schema_version=schema, base_url=baseurl)
    success = service.upload(path, stop)

    if not success:
        sys.exit(os.EX_SOFTWARE)
    else:
        sys.exit(os.EX_OK)

# DOWNLOAD
########################################################################################################################

@cli.command(help='Download a DOI XML file from DataCite')
@click_log.simple_verbosity_option(logger)
@click.argument('suffix')
@click.argument('output_dir', type=click.Path(exists=True))
@click.option('--prefix', envvar='DATACITE_PREFIX', help='DataCite account prefix')
@click.option('--login', envvar='DATACITE_LOGIN', help='DataCite account login')
@click.option('--password', envvar='DATACITE_PASSWORD', prompt=True, help='DataCite account password')
@click.option('--test', envvar='DATACITE_TEST', is_flag=True, help='Use the DataCite TEST platform')
@click.option('--schema', envvar='DATACITE_SCHEMA_VERSION', default='4.4', help='DataCite schema version')
@click.option('--baseurl', envvar='DATACITE_BASE_URL', default='http://seismology.resif.fr', help='Base URL for landing pages')
def download(suffix, output_dir, prefix, login, password, test, schema, baseurl):
    service = ServiceDoi(prefix=prefix, login=login, password=password, test=test, schema_version=schema, base_url=baseurl)
    success = service.download(suffix, output_dir)

    if not success:
        sys.exit(os.EX_SOFTWARE)
    else:
        sys.exit(os.EX_OK)


# MAIN
########################################################################################################################

if __name__ == '__main__':
    cli()
