# encoding: utf8
import os
from io import open
from setuptools import find_packages, setup

current_path = os.path.abspath(os.path.dirname(__file__))

try:
    with open(os.path.join(current_path, 'resif_datacite_cli', '__init__.py'), 'r') as f:
        for line in f:
            if line.startswith('__version__'):
                version = line.strip().split('=')[1].strip(' \'"')
                break
        else:
            version = '0.0.1'
except FileNotFoundError:
    version = '0.0.1'

try:
    with open(os.path.join(current_path, 'README.rst'), 'r', encoding='utf-8') as f:
        readme = f.read()
except FileNotFoundError:
    readme = ''

REQUIRES = [
    'click',
    'click-log',
    'datacite',
    'lxml',
    'python-dotenv',
]

description = '''resif-datacite-cli is a command line tool to manage DOI on DataCite. See http://seismology.resif.fr/'''

kwargs = {
    'name': 'resif-datacite-cli',
    'version': version,
    'description': 'Résif DataCite Command line tool',
    'long_description': readme,
    'author': 'Philippe Bollard',
    'author_email': 'philippe.bollard@univ-grenoble-alpes.fr',
    'maintainer': 'Philippe Bollard',
    'maintainer_email': 'philippe.bollard@univ-grenoble-alpes.fr',
    'url': 'https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/resif-datacite-cli',
    'license': 'GPLv3',
    'classifiers': [
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
    'install_requires': REQUIRES,
    'tests_require': ['coverage', 'pytest'],
    'packages': find_packages(exclude=('tests', 'tests.*')),
    'include_package_data': True,
    'entry_points': '''
        [console_scripts]
        resif-datacite-cli=resif_datacite_cli.cli:cli
    ''',

}

setup(**kwargs)